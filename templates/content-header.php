<header>
    <div class="inner">
        <div class="left">
            <?php if( have_rows('navigation_till_vanster', 'options') ): ?>
                <ul>
                    <?php while( have_rows('navigation_till_vanster', 'options') ): the_row();
                        $sidlank = get_sub_field('sidlank');
                        $lanktext = get_sub_field('lanktext');
                        ?>
                        <li>
                            <a class="helvetica regular" href="<?php echo $sidlank; ?>"><?php echo $lanktext; ?></a>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>

        <div class="middle">
            <a href="/">
                <img src="<?php the_field('logotyp', 'options') ?>">
            </a>
        </div>
        <div class="right">
            <?php if( have_rows('navigation_till_hoger', 'options') ): ?>
                <ul>
                    <?php while( have_rows('navigation_till_hoger', 'options') ): the_row();
                        $sidlank = get_sub_field('sidlank');
                        $lanktext = get_sub_field('lanktext');
                        ?>
                        <li>
                            <a class="helvetica regular" href="<?php echo $sidlank; ?>"><?php echo $lanktext; ?></a>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>
        <div class="mobil-menu">
            <div class="line long"></div>
            <div class="line short"></div>
            <p>
                MENY
            </p>
        </div>
    </div>
</header>
<div class="mobil-menu-container">
    <div class="inner">
        <?php if( have_rows('navigation_till_hoger', 'options') ): ?>
            <ul>
                <?php while( have_rows('navigation_till_vanster', 'options') ): the_row();
                    $sidlank = get_sub_field('sidlank');
                    $lanktext = get_sub_field('lanktext');
                    ?>
                    <li>
                        <a class="helvetica regular" href="<?php echo $sidlank; ?>"><?php echo $lanktext; ?></a>
                    </li>
                <?php endwhile; ?>
        <?php endif; ?>
        <?php if( have_rows('navigation_till_hoger', 'options') ): ?>
                <?php while( have_rows('navigation_till_hoger', 'options') ): the_row();
                    $sidlank = get_sub_field('sidlank');
                    $lanktext = get_sub_field('lanktext');
                    ?>
                    <li>
                        <a class="helvetica regular" href="<?php echo $sidlank; ?>"><?php echo $lanktext; ?></a>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>

<a href="#" class="backUp">
    <svg version="1.1" id="Lager_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 28.9" style="enable-background:new 0 0 50 28.9;" xml:space="preserve">
        <polyline class="st0" points="1,15.7 31.8,15.7 31.8,5.5 44.5,15.7 30.8,26.5 "/>
    </svg>
</a>