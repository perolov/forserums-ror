<div class="flexibelt-innehall-container">
    <?php
        if( have_rows('flexibelt_innehall') ):
            while ( have_rows('flexibelt_innehall') ) : the_row();

                if( get_row_layout() == 'hero' ):
                    ?>
                        <div class="topp-hero" style="background:url(<?php the_sub_field('topp_hero_bild'); ?>) no-repeat center; background-size: cover;">
                            <div class="topp-hero-inner">
                                <h1 class="noe-display"><?php the_sub_field('topp_hero_rubrik'); ?></h1>
                            </div>
                        </div>
                    <?php
                elseif( get_row_layout() == 'kontakta_oss' ):
                    ?>
                        <div class="kontakta-oss-hero <?php the_sub_field('kontakta_oss_inverterad'); ?>" style="background:url(<?php the_sub_field('kontakta_oss_hero_bild'); ?>) no-repeat center; background-size: cover;">
                            <div class="kontakta-oss-hero-inner">
                                <h5 class="helvetica">
                                    <?php the_sub_field('kontakta_oss_hero_ingress'); ?>
                                </h5>
                                <h2 class="noe-display">
                                    <?php the_sub_field('kontakta_oss_hero_rubrik'); ?>
                                </h2>
                                <div class="button-container">
                                    <a href="<?php the_sub_field('knapplank'); ?>">
                                        <span>
                                            <?php the_sub_field('knapptext') ?>
                                        </span>
                                        <svg class="animation" version="1.1" id="Lager_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 28.9" style="enable-background:new 0 0 50 28.9;" xml:space="preserve">
                                            <polyline class="st0" points="1,15.7 31.8,15.7 31.8,5.5 44.5,15.7 30.8,26.5 "/>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php
                elseif( get_row_layout() == 'referenser' ):
                    ?>
                        <div class="referenser-container">
                            <div class="referenser-inner">
                                <?php
                                    if( have_rows('referens') ):
                                        while ( have_rows('referens') ) : the_row();
                                        ?>
                                            <a href="<?php the_sub_field('sidlank'); ?>" class="referens" style="background: url(<?php the_sub_field('bild'); ?>) no-repeat center top; background-size: cover;">
                                                <div class="filter"></div>
                                                <div class="meta">
                                                    <h5 class="helvetica"><?php the_sub_field('vad'); ?></h5>
                                                    <h3 class="noe-display"><?php the_sub_field('namn'); ?></h3>
                                                </div>
                                            </a>
                                        <?php
                                        endwhile;
                                    else :
                                        // no rows found
                                    endif;
                                ?>
                            </div>
                        </div>
                    <?php
                elseif( get_row_layout() == 'text_bild_block' ):
                    ?>
                        <div class="text-bild-block-container <?php the_sub_field('text_bild_inverterad'); ?>">
                            <div class="left">
                                <div class="left-inner">
                                    <h5 class="helvetica"><?php the_sub_field('ingress'); ?></h5>
                                    <h3 class="noe-display"><?php the_sub_field('rubrik'); ?></h3>
                                    <p class="helvetica"><?php the_sub_field('paragraf'); ?></p>
                                    <p></p>
                                </div>
                            </div>
                            <div class="right" style="background:url('<?php the_sub_field('bild'); ?>') no-repeat center; background-size: cover;">
                            </div>
                        </div>
                    <?php
                elseif( get_row_layout() == 'kontaktinfo' ):
                    ?>
                        <div class="kontaktinfo-container" style="background:url('<?php the_sub_field('bild'); ?>') no-repeat center; background-size: cover;">
                            <div class="kontaktinfo-inner" style="background-color:<?php the_sub_field('bakgrundsfarg_block'); ?>">
                                <h1 class="noe-display"><?php the_sub_field('rubrik'); ?></h1>
                                <div class="left">
                                    <div class="form-container">
                                        <?php the_sub_field('formular'); ?>
                                    </div>
                                </div>
                                <div class="right">
                                    <?php the_sub_field('kontaktinfo'); ?>
                                </div>
                            </div>
                        </div>
                    <?php
                elseif( get_row_layout() == 'personal' ):
                    ?>
                        <div class="personal-container">
                            <h1 class="noe-display">
                                <?php the_sub_field('arbetsomrade'); ?>
                            </h1>
                            <?php
                                if( have_rows('personal_person') ):
                                    while ( have_rows('personal_person') ) : the_row();
                                    ?>
                                        <div class="personal">
                                            <?php if( get_sub_field('bild') ): ?>
                                              <img src="<?php the_sub_field('bild'); ?>" alt="personalbild">
                                            <?php endif; ?>
                                            <h4 class="noe-display">
                                                <?php the_sub_field('namn'); ?>
                                            </h4>
                                            <p>
                                                <?php the_sub_field('telefonnummer'); ?>
                                            </p>
                                        </div>
                                    <?php
                                    endwhile;
                                else :
                                    // no rows found
                                endif;
                            ?>
                        </div>
                    <?php

                elseif( get_row_layout() == 'textblock' ):
                    ?>
                        <div class="textblock-container">
                            <div class="inner">
                                <h5 class="helvetica"><?php the_sub_field('ingress'); ?></h5>
                                <h2 class="noe-display"><?php the_sub_field('rubrik'); ?></h2>
                                <p class="helvetica"><?php the_sub_field('paragraf'); ?></p>
                            </div>
                        </div>
                    <?php

                endif;
            endwhile;
        else :
            // no layouts found
        endif;
    ?>
</div>
