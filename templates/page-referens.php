<main class="referens-subpage">
    <div class="left">
        <div class="left-inner">
            <h5 class="helvetica">
                <?php the_field('superior_rubrik'); ?>
            </h5>
            <h1 class="noe-display">
                <?php the_field('rubrik'); ?>
            </h1>
            <p class="ingress">
                <?php the_field('ingress_2'); ?>
            </p>
            <div class="text-container">
                <?php the_field('text'); ?>
            </div>
        </div>
        <img src="<?php the_field('hero_referens_undersida'); ?>" alt="">
    </div>
    <!-- <div class="right" style="background:url(<?php the_field('hero_referens_undersida'); ?>) no-repeat center top; background-size: cover;">
    </div> -->
    <div class="fler-referenser">

        <h2 class="noe-display">Fler referenser</h2>

        <?php 
            $posts = get_field('fler_referenser');
            if( $posts ): ?>
                <div class="referenser-inner">
                    <?php foreach( $posts as $post): ?>
                        <?php setup_postdata($post); ?>
                        <a class="referens" href="<?php the_permalink(); ?>" style="background:url(<?php the_field('hero_referens_undersida'); ?>) no-repeat center; background-size: cover;">
                            <div class="filter"></div>
                            <div class="meta">
                                <h5 class="helvetica">
                                    <?php the_field('superior_rubrik'); ?>
                                </h5>
                                <h3 class="noe-display">
                                    <?php the_field('rubrik'); ?>
                                </h3>
                            </div>
                        </a>
                    <?php endforeach; ?>
                </div>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
</main>