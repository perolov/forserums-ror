<main class="front" style="background:url(<?php the_field('bakgrundsbild'); ?>) no-repeat; background-size: cover; background-position: center bottom;">
	<div class="filter"></div>
	<div class="inner">
		<h1 class="noe-display">
			<?php the_field('startsida_rubrik'); ?>
		</h1>
		<div class="button-container mobile-button">
			<a class="animation" href="#">
				Beställ service
				<svg version="1.1" id="Lager_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 28.9" style="enable-background:new 0 0 50 28.9;" xml:space="preserve">
					<polyline class="st0" points="1,15.7 31.8,15.7 31.8,5.5 44.5,15.7 30.8,26.5 "/>
				</svg>
			</a>
		</div>
	</div>

	<div class="outer">
		<div class="cta-block">
			<div class="mobile-close"></div>
			<button class="primary-btn">
				<span class="open">
					Beställ service
					<svg version="1.1" id="Lager_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 28.9" style="enable-background:new 0 0 50 28.9;" xml:space="preserve">
						<polyline class="st0" points="1,15.7 31.8,15.7 31.8,5.5 44.5,15.7 30.8,26.5 "/>
					</svg>
				</span>
				<span class="close">
					Stäng
					<svg class="animation" version="1.1" id="Lager_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"	viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
						<g id="Atoms-_x2F_-Icons-_x2F_-Close">
							<g id="Icon">
								<path id="path-1_1_" class="st0" d="M45.3,0L50,4.7L4.7,50L0,45.3L45.3,0z M30,34.6l4.6-4.6L50,45.4L45.4,50L30,34.6z M4.6,0
									L20,15.4L15.4,20L0,4.6L4.6,0z"/>
							</g>
						</g>
					</svg>
				</span>
			</button>
			<h4 class="noe-display">Dags att checka värmepumpen</h4>
			<div class="form-container">
				<?php echo do_shortcode( ' [contact-form-7 id="29" title="cta"] ' ); ?>
			</div>	
		</div>
	</div>

</main>
