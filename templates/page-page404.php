<main class="page-not-found">
	<div class="inner">
		<h1 class="noe-display">404 - Rör.. inte denna sidan</h1>
		<p class="helvetica paragraf">Tyvärr, sidan <span class="url"><?php echo $_SERVER['REQUEST_URI']; ?></span> kunde inte hittas.</p>
		<div class="button-container">
			<a class="animation" href="/">
				Gå tillbaka
				<svg version="1.1" id="Lager_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 28.9" style="enable-background:new 0 0 50 28.9;" xml:space="preserve">
					<polyline class="st0" points="1,15.7 31.8,15.7 31.8,5.5 44.5,15.7 30.8,26.5 "/>
				</svg>
			</a>
		</div>
	</div>
</main>